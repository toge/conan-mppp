[settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=10.2
    os=Linux

[requires]
    mppp/0.23

[options]


[full_settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=10.2
    os=Linux

[full_requires]
    gmp/6.2.1:c69edad12af32b57930a572321a86ad8733b8c56
    mppp/0.23@toge/stable:305bb15c9d95ef429657b69b4e8ad4a43f284b96

[full_options]
    gmp:disable_assembly=True
    gmp:enable_cxx=True
    gmp:enable_fat=False
    gmp:fPIC=True
    gmp:run_checks=False
    gmp:shared=False
    mppp:shared=False
    mppp:with_boost=False
    mppp:with_mpc=False
    mppp:with_mpfr=False

[recipe_hash]


[env]

